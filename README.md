# Rust code size exercise

Super simple embedded Linux MCU-manipulating program for code size reduction fun.

## Things I've tried

- stripping the binary (post-build so `cargo bloat` can work)
- aborting on panic (`panic = "abort"` in `Cargo.toml`)
- link-time optimisation (`lto = true` in `Cargo.toml`)
- size optimisation (`opt-level = "z"` in `Cargo.toml`)
- single codegen unit (`codegen-units = 1` in `Cargo.toml`)

None of these brought it below around 350 kB.

- building std to optimise for size ~~can't do, requires nightly~~ I folded on this, see later points

This brought it down to around 310 kB _depending on nightly version_.
One of the surprising things I found was the massive difference different nightly versions can make:

- `nightly-2023-07-01` and `build-std`: 313,328 B.
- `nightly-2023-07-03` and `build-std`: 342,000 B.

If I'd done that experiment only on the second day, I'd have figured it didn't help much.
Fortunately I did it three days before as well, and remembered that it had a noticeable impact.
(The difference occurs across stable versions too, eg. 1.63 vs 1.70 differ by about 10 kB.)

At this point someone on Discord suggested I try to force `flate2` to use `zlib` instead of `miniz_oxide` so that it uses the system zlib.
Adding:

```toml
flate2 = { version = "1.0", default-features = false, features = ["zlib"] }
```

...to `Cargo.toml` did nothing (as in, `miniz_oxide` symbols were still in the binary).
But that led me to look into _why_ they were there.
It looked like they were coming from `std`.
Does `std` use `flate2` and force the default features on (thus enabling `miniz_oxide`)?
No, but it DID depend on `miniz_oxide` directly, via the `backtrace` module/crate.
And it looked like this was a feature flag.
And I was building `std` anyway, so why not try:

- building std with `backtrace` disabled

OOF.
This brings it down to around **81kB**.

I have my price, clearly, because now I'm considering this approach, perhaps via [Xargo](https://github.com/japaric/xargo).

## Building

The build process is implemented in [`build/`](./build).
Since the "real" project deploys on OpenWRT, the build process here uses OpenWRT's toolchain.
If that permits or rules out certain options, that's part of the exercise.
~~The only big rule is that only stable features/toolchains/components are allowed.~~

Building goes in two stages:

1. Build the Docker container
2. Build the binary

The Docker image is based on OpenWRT's buildbot image, with rustup installed.
`RUSTC_TARGET_ARCH` is set explicitly even though the base image already constrains it.
`Makefile-env` is build to populate a file with environment variables, effectively exporting them from OpenWRT's build system so `cargo` can use them.
In a real OpenWRT project this would be integrated as a package.
Here it is not so that it's easier to prototype changes to the Rust build settings.

The Docker image also has `cargo bloat` pre-installed and maybe any other things that take a long time so it's not eating up CI time.

The full steps are: 

```none
(cd build && docker build -t rust-size-test .)
docker run \
  --rm --name rust-size \
  --mount type=bind,src=.,dst=/crate \
  --workdir=/crate \
  -it rust-size-test \
  bash
# In docker container
export TARGET_NAME=prod-self-test
build/build.sh
# Or, here you can experiment with different build commands.
```
