#!/bin/sh

set -e
set -u

# Build script for Rust code size exercise. Run this from the crate's root
# directory ie. where you would normally run 'cargo build'.

export CARGO_HOME="$HOME/$CARGO_HOME_EXTRA"
export RUSTUP_HOME="$HOME/$RUSTUP_HOME_EXTRA"

# Source this first, it clobbers PATH by design.
. "$HOME/env.txt"
. "$CARGO_HOME/env"

# The Make variable REAL_GNU_TARGET_NAME is the prefix for all the
# toolchain binaries we're using ie. "mipsel-openwrt-linux-musl". Setting
# CROSS_COMPILE in the shell here is picked up by Rust's cc package, so it
# knows to use mipsel-openwrt-linux-musl-gcc instead of its internal
# fallback. This is important for "-sys" crates like openssl-sys that need
# to build C projects as dependencies.
export CROSS_COMPILE="$REAL_GNU_TARGET_NAME"
export RUSTFLAGS="-C linker=$TARGET_CC_NOCACHE -C ar=$TARGET_AR"

# These should be shared between the 'build' and 'bloat' commands.
CARGO_FLAGS="--release \
  -Z build-std=std,panic_abort -Z build-std-features \
  --target $RUSTC_TARGET_ARCH"

# RUSTC_TARGET_ARCH should be set by the Docker image.
"$CARGO_HOME/bin/cargo" build $CARGO_FLAGS

"$TARGET_STRIP" -o /tmp/$TARGET_NAME ./target/$RUSTC_TARGET_ARCH/release/$TARGET_NAME
stat --printf '\n----\nStripped size: %s\n----\n\n' /tmp/$TARGET_NAME

"$CARGO_HOME/bin/cargo" bloat $CARGO_FLAGS
