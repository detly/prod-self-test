#!/bin/sh

set -e
set -u

export CARGO_HOME="$HOME/$CARGO_HOME_EXTRA"
export RUSTUP_HOME="$HOME/$RUSTUP_HOME_EXTRA"

RUSTUP_URL="https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init"

RUSTUP_DIR="$HOME/rustup"
RUSTUP_SCRIPT="$RUSTUP_DIR/rustup-init"

RUSTUP_INIT_ARGS="\
    --no-modify-path \
    --profile default \
    --default-toolchain $RUST_VER \
    --target $RUSTC_TARGET_ARCH"

CONFIGURE_VARS="\
    ac_cv_path_CARGO=\"$CARGO_HOME/bin/cargo\" \
    ac_cv_path_RUSTC=\"$CARGO_HOME/bin/rustc\""

mkdir -p "$RUSTUP_DIR"
cd "$RUSTUP_DIR"

curl --proto '=https' --tlsv1.2 -sSf "$RUSTUP_URL" -o "$RUSTUP_SCRIPT"
chmod a+x "$RUSTUP_SCRIPT"

# No quoting on args
env "$CONFIGURE_VARS" "$RUSTUP_SCRIPT" -y -v $RUSTUP_INIT_ARGS

. "$CARGO_HOME/env"
rustup component add rust-src
cargo install cargo-bloat
