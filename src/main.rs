use gpio_cdev::LineRequestFlags;
use std::{eprintln, process::ExitCode, time::Duration};

const MCU_GPIO: &str = "/dev/gpiochip1";
const MCU_BOOT_LINE: u32 = 5;
const MCU_NRST_LINE: u32 = 4;
const MCU_NRST_TIME: Duration = Duration::from_millis(50);
const MCU_BOOT_TIME: Duration = Duration::from_millis(200);

struct Mcu {
    name: &'static str,
    boot: gpio_cdev::Line,
    nrst: gpio_cdev::Line,
}

impl Mcu {
    fn new(name: &'static str) -> Result<Self, gpio_cdev::Error> {
        let mut chip = gpio_cdev::Chip::new(MCU_GPIO)?;

        let boot = chip.get_line(MCU_BOOT_LINE)?;
        let nrst = chip.get_line(MCU_NRST_LINE)?;

        let mcu = Self { name, boot, nrst };

        mcu.restore_gpios()?;

        Ok(mcu)
    }

    fn restore_gpios(&self) -> Result<(), gpio_cdev::Error> {
        self.boot.request(LineRequestFlags::INPUT, 0, self.name)?;
        self.nrst.request(LineRequestFlags::INPUT, 0, self.name)?;
        Ok(())
    }

    fn reset_to_bootloader(&self) -> Result<(), gpio_cdev::Error> {
        self.boot
            .request(LineRequestFlags::OUTPUT, 1, self.name)?
            .set_value(1)?;

        self.nrst
            .request(LineRequestFlags::OUTPUT, 0, self.name)?
            .set_value(0)?;
        std::thread::sleep(MCU_NRST_TIME);
        self.nrst.request(LineRequestFlags::INPUT, 0, self.name)?;

        std::thread::sleep(MCU_BOOT_TIME);
        self.boot.request(LineRequestFlags::INPUT, 0, self.name)?;

        Ok(())
    }
}

impl Drop for Mcu {
    fn drop(&mut self) {
        self.restore_gpios().expect("Could not restore GPIO state");
    }
}

fn main_inner() -> Result<(), gpio_cdev::Error> {
    let consumer_name = env!("CARGO_PKG_NAME");
    let mcu = Mcu::new(consumer_name)?;
    mcu.reset_to_bootloader()?;
    Ok(())
}

fn main() -> ExitCode {
    if let Err(error) = main_inner() {
        eprintln!("Error running test.");
        eprintln!("{error:#}");
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}
